

# Łukasz Reinke s15037

import cv2
import numpy as np

# Funkcja do wykrywania twarzy i rysowania celownika
def detect_and_target_face(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    for (x, y, w, h) in faces:
        # Sprawdzenie koloru w centrum twarzy
        face_center_color = image[y + h // 2, x + w // 2]

        # Sprawdzenie, czy kolor jest zielony
        if is_color_green(face_center_color):
            # Rysowanie celownika
            cv2.circle(image, (x + w // 2, y + h // 2), min(w, h) // 2, (0, 255, 0), 2)

# Funkcja do sprawdzania, czy kolor jest zielony
def is_color_green(bgr_color):
    # Przekształcanie pojedynczego piksela w obraz 1x1x3
    bgr_color = np.uint8([[bgr_color]])

    # Konwersja koloru z BGR na HSV
    hsv_color = cv2.cvtColor(bgr_color, cv2.COLOR_BGR2HSV)

    # Zakres koloru zielonego w przestrzeni HSV
    lower_green = np.array([40, 40, 40])
    upper_green = np.array([80, 255, 255])

    # Sprawdzenie, czy kolor mieści się w zakresie zielonego
    mask = cv2.inRange(hsv_color, lower_green, upper_green)
    return np.any(mask)

# Inicjalizacja kamery
cap = cv2.VideoCapture(0)

# Pętla do przechwytywania obrazu z kamery
while True:
    ret, frame = cap.read()
    if not ret:
        break

    # Przetworzenie obrazu
    detect_and_target_face(frame)

    # Wyświetlenie obrazu
    cv2.imshow('Camera Feed - Targeted Faces', frame)

    # Warunek na zamknięcie okna
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Zwolnienie kamery i zamknięcie wszystkich okien
cap.release()
cv2.destroyAllWindows()

